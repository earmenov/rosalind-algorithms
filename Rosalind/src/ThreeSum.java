import java.util.*;

class ThreeSum {

    String existsThreeSum(List<Integer> list) {
        Set<Integer> container = new HashSet<>(list);
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (container.contains((list.get(i) + list.get(j)) * -1)) {
                    List<Integer> result = new ArrayList<>(3);
                    result.add(i + 1);
                    result.add(j + 1);
                    result.add(list.indexOf((list.get(i) + list.get(j)) * -1) + 1);
                    Collections.sort(result);
                    return String.format("%d %d %d", result.get(0), result.get(1), result.get(2));
                }
            }
        }
        return "-1";
    }
}

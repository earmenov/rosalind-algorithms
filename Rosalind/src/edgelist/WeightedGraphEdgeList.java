package edgelist;

import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

public class WeightedGraphEdgeList extends EdgeList<Map<Pair<Integer,Integer>, Integer>> {

    public WeightedGraphEdgeList() {
        super(new HashMap<>());
    }

    @Override
    public WeightedGraphEdgeList createEdgeListFromConsole(Scanner scanner) {
        setNumOfVertices(scanner.nextInt());
        setNumOfEdges(scanner.nextInt());

        for (int i = 0; i < numOfEdges; i++) {
            int parent = scanner.nextInt();
            int child = scanner.nextInt();
            int weight = scanner.nextInt();
            edgeList.put(Pair.of(parent, child), weight);
        }

        return this;
    }

    public GraphEdgeList getEdgeListNoWeights() {
        return new GraphEdgeList(getNumOfVertices(), getNumOfEdges(), new ArrayList<>(getEdgeList().keySet()));
    }
}

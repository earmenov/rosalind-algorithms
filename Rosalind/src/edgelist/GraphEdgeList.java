package edgelist;

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GraphEdgeList extends EdgeList<List<Pair<Integer,Integer>>> {

    public GraphEdgeList() {
        super(new ArrayList<>());
    }

    public GraphEdgeList(int numOfVertices, int numOfEdges, List<Pair<Integer,Integer>> edgeList) {
        super(numOfVertices, numOfEdges, edgeList);
    }

    @Override
    public GraphEdgeList createEdgeListFromConsole(Scanner scanner) {
        setNumOfVertices(scanner.nextInt());
        setNumOfEdges(scanner.nextInt());

        for (int i = 0; i < numOfEdges; i++) {
            int parent = scanner.nextInt();
            int child = scanner.nextInt();
            edgeList.add(Pair.of(parent, child));
        }

        return this;
    }
}

package edgelist;

import java.util.Scanner;

public abstract class EdgeList<T> {
    protected int numOfVertices;
    protected int numOfEdges;
    protected T edgeList;

    EdgeList(T collection) {
        this.numOfVertices = 0;
        this.numOfEdges = 0;
        this.edgeList = collection;
    }

    EdgeList(int numOfVertices, int numOfEdges, T collection) {
        this.numOfVertices = numOfVertices;
        this.numOfEdges = numOfEdges;
        this.edgeList = collection;
    }

    public abstract EdgeList<T> createEdgeListFromConsole(Scanner scanner);

    public int getNumOfVertices() {
        return numOfVertices;
    }

    protected void setNumOfVertices(int numOfVertices) {
        if (numOfVertices <= 0) {
            throw new IllegalArgumentException("Can not create a graph with zero or negative size vertices");
        }

        this.numOfVertices = numOfVertices;
    }

    public int getNumOfEdges() {
        return numOfEdges;
    }

    protected void setNumOfEdges(int numOfEdges) {
        if (numOfEdges <= 0) {
            throw new IllegalArgumentException("Can not create a graph with zero or negative size vertices");
        }

        this.numOfEdges = numOfEdges;
    }

    public T getEdgeList() {
        return edgeList;
    }

    protected void setEdgeList(T edgeList) {
        this.edgeList = edgeList;
    }
}

import edgelist.GraphEdgeList;
import edgelist.WeightedGraphEdgeList;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private final Scanner scanner;

    Main(Scanner scanner) {
        this.scanner = scanner;
    }

    void solveQuickSort() {
        QuicklySorted sorted = new QuicklySorted();
        int size = scanner.nextInt();
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            array.add(scanner.nextInt());
        }
        sorted.quickSort(array, 0, array.size() - 1);
        for (int el : array) {
            System.out.print(el + " ");
        }
    }

    void solveMedian() {
        QuicklySorted sorted = new QuicklySorted();
        int size = scanner.nextInt();
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            array.add(scanner.nextInt());
        }
        int k = scanner.nextInt();
        int kSmallest = sorted.quickSelect(array, 0, array.size() - 1, k - 1);
        System.out.println(kSmallest);
    }

    void solveThreeSum() {
        int rows = scanner.nextInt();
        int cols = scanner.nextInt();
        ThreeSum threeSum = new ThreeSum();

        for (int i = 0; i < rows; i++) {
            List<Integer> list = new ArrayList<>(cols);
            for (int j = 0; j < cols; j++) {
                list.add(scanner.nextInt());
            }
            System.out.println(threeSum.existsThreeSum(list));
        }
    }

    void solveTwoWayPartition() {
        QuicklySorted sorted = new QuicklySorted();
        int size = scanner.nextInt();
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            array.add(scanner.nextInt());
        }
        sorted.twoWayPartition(array, 0, array.size() - 1, 0);
        for (int el : array) {
            System.out.print(el + " ");
        }
    }

    void solveThreeWayPartition() {
        QuicklySorted sorted = new QuicklySorted();
        int size = scanner.nextInt();
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            array.add(scanner.nextInt());
        }

        for (int el : sorted.threeWayPartition(array, 0, array.size(), 0)) {
            System.out.print(el + " ");
        }
    }

    void solveAcyclicity() {
        int numOfGraphs = scanner.nextInt();

        for (int i = 0; i < numOfGraphs; i++) {
            System.out.print((GraphOperator
                    .isGraphAcyclic(new GraphEdgeList().createEdgeListFromConsole(scanner))) ? "1 " : "-1 ");
        }
    }

    void solveTopologicalSort() {
        List<Integer> sortedVertices = GraphOperator
                .sortDAGTopologically(new GraphEdgeList().createEdgeListFromConsole(scanner));
        sortedVertices.forEach(vertex -> System.out.print(vertex + " "));
    }

    void solveSquareInAGraph() {
        int numOfGraphs = scanner.nextInt();

        for (int i = 0; i < numOfGraphs; i++) {
            System.out.print((GraphOperator
                    .hasGraphSquare(new GraphEdgeList().createEdgeListFromConsole(scanner))) ? "1 " : "-1 ");
        }
    }

    void solveIsBipartite() {
        int numOfGraphs = scanner.nextInt();
        for (int i = 0; i < numOfGraphs; i++) {
            System.out.print((GraphOperator
                    .isBipartite(new GraphEdgeList().createEdgeListFromConsole(scanner))) ? "1 " : "-1 ");
        }
    }

    void solveBellmanFord() {
        List<Long> result = GraphOperator.bellmanFordAlgorithm(new WeightedGraphEdgeList().createEdgeListFromConsole(scanner), 1);
        for (long distance : result) {
            System.out.print((distance == Integer.MAX_VALUE) ? "x " : distance + " ");
        }
    }

    void solveShortestPathsInDAG() {
        List<Long> result = GraphOperator.getShortestPathsInDAG(new WeightedGraphEdgeList().createEdgeListFromConsole(scanner), 1);
        for (long distance : result) {
            System.out.print((distance == Integer.MAX_VALUE) ? "x " : distance + " ");
        }
    }

    void solveDijkstra() {
        List<Long> result = GraphOperator.dijkstra(new WeightedGraphEdgeList().createEdgeListFromConsole(scanner), 1);
        for (long distance : result) {
            System.out.print((distance == Integer.MAX_VALUE) ? "-1 " : distance + " ");
        }
    }

    void solveGraphNegativeCycles() {
        int numOfGraphs = scanner.nextInt();
        for (int i = 0; i < numOfGraphs; i++) {
            System.out.print((GraphOperator
                    .hasNegativeCycle(new WeightedGraphEdgeList().createEdgeListFromConsole(scanner))) ? "1 " : "-1 ");
        }
    }

    void solveHamiltonPath() {
        int numOfGraphs = scanner.nextInt();
        for (int i = 0; i < numOfGraphs; i++) {
            List<Integer> hamiltonPath =
                    GraphOperator.findHamiltonPath(new GraphEdgeList().createEdgeListFromConsole(scanner));
            if (hamiltonPath != null) {
                System.out.print("1 ");
                for (int vertex : hamiltonPath) {
                    System.out.print(vertex + " ");
                }
                System.out.println();
            } else {
                System.out.println("-1");
            }
        }
    }

    void solveSCC() {
        System.out.println(GraphOperator.countSCC(new GraphEdgeList().createEdgeListFromConsole(scanner)));
    }

    void solveGeneralSink() {
        int numOfGraphs = scanner.nextInt();
        for (int i = 0; i < numOfGraphs; i++) {
            System.out.print(GraphOperator
                    .findMotherVertex(new GraphEdgeList().createEdgeListFromConsole(scanner)) + " ");
        }
    }

    void solveSemiConnected() {
        int numOfGraphs = scanner.nextInt();
        for (int i = 0; i < numOfGraphs; i++) {
            System.out.print((GraphOperator
                    .findMotherVertex(new GraphEdgeList().createEdgeListFromConsole(scanner)) != -1) ? "1 " : "-1 ");
        }
    }

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            Main solver = new Main(sc);
            solver.solveDijkstra();
        }
    }
}

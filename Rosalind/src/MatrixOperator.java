import java.util.ArrayList;
import java.util.List;

class MatrixOperator {

    List<List<Integer>> power(List<List<Integer>> m1, int power) {
        if (power < 0) {
            throw new IllegalArgumentException("Power can not be less than zero");
        }

        List<List<Integer>> result = m1;
        for (int i = 1; i < power; i++) {
            result = multiply(result, m1);
        }

        return result;
    }

    List<List<Integer>> multiply(List<List<Integer>> m1, List<List<Integer>> m2) {
        if (m1.get(0).size() != m2.size()) {
            throw new IllegalArgumentException();
        }

        List<List<Integer>> result = new ArrayList<>();
        for (int k = 0; k < m1.size(); k++) {
            result.add(new ArrayList<>());
            for (int i = 0; i < m1.size(); i++) {
                int n = 0;
                for (int j = 0; j < m2.get(0).size(); j++) {
                    n += m1.get(k).get((j)) * m2.get(j).get(i);
                }
                result.get(k).add(n);
            }
        }
        return result;
    }

    List<List<Integer>> sum(List<List<Integer>> m1, List<List<Integer>> m2) {
        if (m1.size() != m2.size() && m1.get(0).size() != m2.get(0).size()) {
            throw new IllegalArgumentException();
        }

        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i < m1.size(); i++) {
            result.add(new ArrayList<>());
            for (int j = 0; j < m2.get(0).size(); j++) {
                result.get(i).add(m1.get(i).get(j) + m2.get(i).get(j));
            }
        }

        return result;
    }

    List<List<Integer>> subtract(List<List<Integer>> m1, List<List<Integer>> m2) {
        if (m1.size() != m2.size() && m1.get(0).size() != m2.get(0).size()) {
            throw new IllegalArgumentException();
        }

        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i < m1.size(); i++) {
            result.add(new ArrayList<>());
            for (int j = 0; j < m2.get(0).size(); j++) {
                result.get(i).add(m1.get(i).get(j) - m2.get(i).get(j));
            }
        }

        return result;
    }

    List<List<Integer>> getDiagOfMatrix(List<List<Integer>> matrix) {
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i < matrix.size(); i++) {
            result.add(new ArrayList<>());
            for (int j = 0; j < matrix.get(0).size(); j++) {
                result.get(i).add((i == j) ? matrix.get(i).get(j) : 0);
            }
        }

        return result;
    }

    List<List<Integer>> getNonDiagOfMatrix(List<List<Integer>> matrix) {
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i < matrix.size(); i++) {
            result.add(new ArrayList<>());
            for (int j = 0; j < matrix.get(0).size(); j++) {
                result.get(i).add((i != j) ? matrix.get(i).get(j) : 0);
            }
        }

        return result;
    }
}

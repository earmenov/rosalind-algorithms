import edgelist.GraphEdgeList;
import edgelist.WeightedGraphEdgeList;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.stream.Collectors;

class GraphOperator {

    static Integer findMotherVertex(GraphEdgeList graphEdgeList) {
        Map<Integer, Set<Integer>> graph = createGraphAdjList(graphEdgeList, true);

        List<Integer> path = new ArrayList<>();
        List<Boolean> visited = new ArrayList<>(Collections.nCopies(graphEdgeList.getNumOfVertices(), false));

        for (int i = 0; i < graphEdgeList.getNumOfVertices(); i++) {
            if (!graph.containsKey(i + 1)) {
                return -1;
            }
            if (!visited.get(i)) {
                path = dfs(graph, i + 1);
                if (path.size() == graphEdgeList.getNumOfVertices()) {
                    return i + 1;
                }
            }
            for (int vertex : path) {
                visited.set(vertex - 1, true);
            }
        }

        return -1;
    }

    static int countSCC(GraphEdgeList graphEdgeList) {
        final int UNVISITED = -1;

        Map<Integer, Set<Integer>> graph = createGraphAdjList(graphEdgeList, true);
        List<Integer> vertexIds = new ArrayList<>(Collections.nCopies(graphEdgeList.getNumOfVertices(), UNVISITED));
        List<Integer> vertexLowLinks = new ArrayList<>(Collections.nCopies(graphEdgeList.getNumOfVertices(), 0));
        List<Boolean> inStack = new ArrayList<>(Collections.nCopies(graphEdgeList.getNumOfVertices(), false));
        Stack<Integer> currVertices = new Stack<>();

        MutableInt id = new MutableInt();
        int sccCount = 0;

        for (int i = 0; i < graphEdgeList.getNumOfVertices(); i++) {
            if (!graph.containsKey(i + 1)) {
                sccCount++;
                continue;
            }
            if (vertexIds.get(i) == UNVISITED) {
                sccCount = tarjanAlgorithm(graph, vertexIds, vertexLowLinks, currVertices, inStack, id, i, sccCount);
            }
        }
        return sccCount;
    }

    static List<Integer> findHamiltonPath(GraphEdgeList graphEdgeList) {
        Map<Integer, Set<Integer>> graph = createGraphAdjList(graphEdgeList, true);

        List<Integer> sortedVertices = sortDAGTopologically(graphEdgeList);
        List<Integer> hamiltonPath = new ArrayList<>();

        for (int i = 0; i < sortedVertices.size() - 1; i++) {
            if (graph.get(sortedVertices.get(i)).contains(sortedVertices.get(i + 1))) {
                hamiltonPath.add(sortedVertices.get(i));
            } else {
                return null;
            }
        }

        hamiltonPath.add(sortedVertices.get(sortedVertices.size() - 1));
        return hamiltonPath;
    }

    static List<Long> getShortestPathsInDAG(WeightedGraphEdgeList weightedEdgeList, int startVertex) {
        Map<Integer, Set<AbstractMap.SimpleEntry<Integer, Integer>>> graph
                = createGraphWithEdgeWeights(weightedEdgeList, true);
        List<Long> distances = new ArrayList<>(Collections.nCopies(weightedEdgeList.getNumOfVertices(), (long) Integer.MAX_VALUE));
        distances.set(startVertex - 1, 0L);
        List<Integer> sortedVertices = sortDAGTopologically(weightedEdgeList.getEdgeListNoWeights());

        for (int vertex : sortedVertices) {
            for (AbstractMap.SimpleEntry<Integer, Integer> entry : graph.get(vertex)) {
                if (!distances.get(vertex - 1).equals((long) Integer.MAX_VALUE)) {
                    long newDistance = distances.get(vertex - 1) + entry.getValue();
                    if (newDistance < distances.get(entry.getKey() - 1)) {
                        distances.set(entry.getKey() - 1, newDistance);
                    }
                }
            }
        }

        return distances;
    }

    static List<Long> bellmanFordAlgorithm(WeightedGraphEdgeList weightedGraphEdgeList, int startVertex) {
        Map<Integer, Set<AbstractMap.SimpleEntry<Integer, Integer>>> graph = createGraphWithEdgeWeights(weightedGraphEdgeList, true);

        List<Long> distances = new ArrayList<>(Collections.nCopies(weightedGraphEdgeList.getNumOfVertices(), (long) Integer.MAX_VALUE));
        distances.set(startVertex - 1, 0L);
        for (int i = 0; i < graph.keySet().size() - 1; i++) {
            boolean hasDistancePathChange = false;
            for (int vertex : graph.keySet()) {
                for (AbstractMap.SimpleEntry<Integer, Integer> entry : graph.get(vertex)) {
                    if (!distances.get(vertex - 1).equals((long) Integer.MAX_VALUE)) {
                        long newDistance = distances.get(vertex - 1) + entry.getValue();
                        if (newDistance < distances.get(entry.getKey() - 1)) {
                            distances.set(entry.getKey() - 1, newDistance);
                            hasDistancePathChange = true;
                        }
                    }
                }
            }
            if (!hasDistancePathChange) {
                break;
            }
        }

        return distances;
    }

    static List<Long> dijkstra(WeightedGraphEdgeList weightedGraphEdgeList, int startVertex) {
        Map<Integer, Set<AbstractMap.SimpleEntry<Integer, Integer>>> graph = createGraphWithEdgeWeights(weightedGraphEdgeList, true);
        Queue<Pair<Integer, Long>> pQueue = new PriorityQueue<>(Map.Entry.comparingByValue());
        pQueue.add(Pair.of(startVertex, 0L));
        List<Long> distances = new ArrayList<>(Collections.nCopies(weightedGraphEdgeList.getNumOfVertices(), (long) Integer.MAX_VALUE));
        distances.set(startVertex - 1, 0L);
        Set<Integer> visited = new HashSet<>();

        while (!pQueue.isEmpty()) {
            int currNode = pQueue.poll().getKey();
            visited.add(currNode);
            for (AbstractMap.SimpleEntry<Integer, Integer> neighbour : graph.get(currNode)) {
                if (!visited.contains(neighbour.getKey())) {
                    long newDistance = distances.get(currNode - 1) + neighbour.getValue();
                    if (distances.get(neighbour.getKey() - 1) > newDistance) {
                        pQueue.add(Pair.of(neighbour.getKey(),  newDistance));
                        distances.set(neighbour.getKey() - 1, newDistance);
                    }
                }
            }
        }

        return distances;
    }

    static boolean hasNegativeCycle(WeightedGraphEdgeList weightedGraphEdgeList) {
        Map<Integer, Set<AbstractMap.SimpleEntry<Integer, Integer>>> graph = createGraphWithEdgeWeights(weightedGraphEdgeList, true);

        List<Boolean> visited = new ArrayList<>(Collections.nCopies(weightedGraphEdgeList.getNumOfVertices(), false));
        List<Long> distances = new ArrayList<>();
        for (int i = 0; i < weightedGraphEdgeList.getNumOfVertices(); i++) {
            if (!visited.get(i)) {
                distances = bellmanFordAlgorithm(weightedGraphEdgeList, i + 1);

                for (int vertex : graph.keySet()) {
                    for (AbstractMap.SimpleEntry<Integer, Integer> entry : graph.get(vertex)) {
                        if (!distances.get(vertex - 1).equals((long) Integer.MAX_VALUE)) {
                            long newDistance = distances.get(vertex - 1) + entry.getValue();
                            if (newDistance < distances.get(entry.getKey() - 1)) {
                                distances.set(entry.getKey() - 1, newDistance);
                                return true;
                            }
                        }
                    }
                }
            }

            for (int j = 0; j < weightedGraphEdgeList.getNumOfVertices(); j++) {
                if (distances.get(j) != Integer.MAX_VALUE) {
                    visited.set(j, true);
                }
            }
        }

        return false;
    }

    static boolean isBipartite(GraphEdgeList graphEdgeList) {
        Map<Integer, Set<Integer>> graph = GraphOperator.createGraphAdjList(graphEdgeList, false);

        Set<Integer> redColor = new HashSet<>();
        Set<Integer> blueColor = new HashSet<>();

        int startVertex = graph.keySet().stream().findAny().orElseThrow();
        redColor.add(startVertex);

        Queue<Integer> queue = new LinkedList<>();
        queue.add(startVertex);

        while (!queue.isEmpty()) {
            int parent = queue.poll();
            for (int child : graph.get(parent)) {
                if (!blueColor.contains(child) && !redColor.contains(child)) {
                    queue.add(child);
                    if (redColor.contains(parent)) {
                        blueColor.add(child);
                    } else if (blueColor.contains(parent)) {
                        redColor.add(child);
                    }
                } else if (blueColor.contains(child) && blueColor.contains(parent) || redColor.contains(child) && redColor.contains(parent)) {
                    return false;
                }
            }
        }
        return true;
    }

    static List<Integer> bfs(Map<Integer, Set<Integer>> graph, int startVertex) {
        Queue<Integer> queue = new LinkedList<>();
        Set<Integer> visited = new HashSet<>();

        queue.add(startVertex);
        visited.add(startVertex);

        List<Integer> resultPath = new ArrayList<>();
        resultPath.add(startVertex);

        while (!queue.isEmpty()) {
            int parent = queue.poll();
            for (int child : graph.get(parent)) {
                if (visited.add(child)) {
                    queue.add(child);
                    resultPath.add(child);
                }
            }
        }

        return resultPath;
    }

    static List<Integer> dfs(Map<Integer, Set<Integer>> graph, int startVertex) {
        Stack<Integer> stack = new Stack<>();
        Set<Integer> visited = new HashSet<>();

        stack.add(startVertex);
        visited.add(startVertex);

        List<Integer> resultPath = new ArrayList<>();
        resultPath.add(startVertex);

        while (!stack.isEmpty()) {
            int parent = stack.pop();
            for (int child : graph.get(parent)) {
                if (!visited.contains(child)) {
                    visited.add(child);
                    stack.add(child);
                    resultPath.add(child);
                }
            }
        }

        return resultPath;
    }

    static boolean hasGraphSquare(GraphEdgeList graphEdgeList) {
        List<List<Integer>> graph = GraphOperator.createGraphAsMatrix(graphEdgeList, false);

        for (int i = 0; i < graph.size(); i++) {
            for (int j = i + 1; j < graph.get(0).size() - 1; j++) {
                for (int k = j + 1; k < graph.get(0).size(); k++) {
                    if (graph.get(i).get(j) == 1 && graph.get(i).get(k) == 1) {
                        for (int l = 0; l < graph.get(0).size(); l++) {
                            if (l == i || l == j || l == k) {
                                continue;
                            }
                            if (graph.get(j).get(l) == 1 && graph.get(k).get(l) == 1) {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    static List<Integer> sortDAGTopologically(GraphEdgeList graphEdgeList) {
        Map<Integer, Set<Integer>> graph = GraphOperator.createGraphAdjList(graphEdgeList, true);
        Map<Integer, Set<Integer>> tempGraph = new HashMap<>();
        graph.forEach((key, value) -> tempGraph.put(key, new HashSet<>(value)));

        Queue<Integer> leaves = tempGraph.entrySet().stream()
                .filter(e -> e.getValue().isEmpty())
                .map(Map.Entry::getKey)
                .collect(Collectors.toCollection(LinkedList::new));
        Map<Integer, Set<Integer>> parentVerticesGraph = createParentVerticesGraph(tempGraph);

        Stack<Integer> sortedNodes = new Stack<>();
        while (!leaves.isEmpty()) {
            int leaf = leaves.poll();
            sortedNodes.add(leaf);
            tempGraph.remove(leaf);
            for (int node : parentVerticesGraph.get(leaf)) {
                tempGraph.get(node).remove(leaf);
                if (tempGraph.get(node).isEmpty()) {
                    leaves.add(node);
                }
            }
        }

        List<Integer> result = new ArrayList<>();
        while (!sortedNodes.isEmpty()) {
            result.add(sortedNodes.pop());
        }

        return result;
    }

    static boolean isGraphAcyclic(GraphEdgeList graphEdgeList) {
        Map<Integer, Set<Integer>> graph = GraphOperator.createGraphAdjList(graphEdgeList, true);

        Queue<Integer> leaves = graph.entrySet().stream()
                .filter(e -> e.getValue().isEmpty())
                .map(Map.Entry::getKey)
                .collect(Collectors.toCollection(LinkedList::new));
        Map<Integer, Set<Integer>> parentVerticesGraph = createParentVerticesGraph(graph);

        while (!leaves.isEmpty()) {
            int leaf = leaves.poll();
            graph.remove(leaf);
            for (int node : parentVerticesGraph.get(leaf)) {
                graph.get(node).remove(leaf);
                if (graph.get(node).isEmpty()) {
                    leaves.add(node);
                }
            }
        }

        return graph.isEmpty();
    }

    static private int tarjanAlgorithm(Map<Integer, Set<Integer>> graph, List<Integer> vertexIds, List<Integer> vertexLowLinks,
                                       Stack<Integer> currVertices, List<Boolean> inStack, MutableInt id, int atVertex, int sccCount) {

        final int UNVISITED = -1;
        currVertices.add(atVertex);
        inStack.set(atVertex, true);
        vertexIds.set(atVertex, id.toInteger());
        vertexLowLinks.set(atVertex, id.toInteger());

        for (int neighbourVertex : graph.get(atVertex + 1)) {
            if (vertexIds.get(neighbourVertex - 1) == UNVISITED) {
                id.increment();
                sccCount = tarjanAlgorithm(graph, vertexIds, vertexLowLinks, currVertices, inStack, id, neighbourVertex - 1, sccCount);
            }
            if (inStack.get(neighbourVertex - 1)) {
                vertexLowLinks.set(atVertex, Math.min(vertexLowLinks.get(atVertex), vertexLowLinks.get(neighbourVertex - 1)));
            }
        }

        if (vertexIds.get(atVertex).equals(vertexLowLinks.get(atVertex))) {
            int node;
            do {
                node = currVertices.pop();
                inStack.set(node, false);
                vertexLowLinks.set(node, vertexIds.get(atVertex));
            } while (node != atVertex);
            sccCount++;
        }

        return sccCount;
    }

    static Map<Integer, Set<Integer>> createGraphAdjList(GraphEdgeList graphEdgeList, boolean directed) {
        Map<Integer, Set<Integer>> graph = new HashMap<>();

        for (Pair<Integer, Integer> pair : graphEdgeList.getEdgeList()) {
            int parent = pair.getKey();
            int child = pair.getValue();
            if (!graph.containsKey(parent)) {
                graph.put(parent, new HashSet<>());
            }
            if (!graph.containsKey(child)) {
                graph.put(child, new HashSet<>());
            }
            graph.get(parent).add(child);
            if (!directed) {
                graph.get(child).add(parent);
            }
        }
        return graph;
    }

    static Map<Integer, Set<AbstractMap.SimpleEntry<Integer, Integer>>> createGraphWithEdgeWeights(WeightedGraphEdgeList weightedGraphEdgeList, boolean directed) {
        Map<Integer, Set<AbstractMap.SimpleEntry<Integer, Integer>>> graph = new HashMap<>();

        for (Map.Entry<Pair<Integer, Integer>, Integer> pair : weightedGraphEdgeList.getEdgeList().entrySet()) {
            int parent = pair.getKey().getKey();
            int child = pair.getKey().getValue();
            int weight = pair.getValue();

            if (!graph.containsKey(parent)) {
                graph.put(parent, new HashSet<>());
            }
            if (!graph.containsKey(child)) {
                graph.put(child, new HashSet<>());
            }
            graph.get(parent).add(new AbstractMap.SimpleEntry<>(child, weight));
            if (!directed) {
                graph.get(child).add(new AbstractMap.SimpleEntry<>(parent, weight));
            }
        }
        return graph;
    }

    static List<List<Integer>> createGraphAsMatrix(GraphEdgeList graphEdgeList, boolean directed) {
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < graphEdgeList.getNumOfVertices(); i++) {
            graph.add(new ArrayList<>(Collections.nCopies(graphEdgeList.getNumOfVertices(), 0)));
        }

        for (Pair<Integer, Integer> pair : graphEdgeList.getEdgeList()) {
            int parent = pair.getKey() - 1;
            int child = pair.getValue() - 1;
            graph.get(parent).set(child, 1);
            if (!directed) {
                graph.get(child).set(parent, 1);
            }
        }
        return graph;
    }

    static private Map<Integer, Set<Integer>> createParentVerticesGraph(Map<Integer, Set<Integer>> graph) {
        Map<Integer, Set<Integer>> parentVerticesGraph = new HashMap<>();
        for (Map.Entry<Integer, Set<Integer>> entry : graph.entrySet()) {
            for (int node : entry.getValue()) {
                if (!parentVerticesGraph.containsKey(node)) {
                    parentVerticesGraph.put(node, new HashSet<>());
                }
                if (!parentVerticesGraph.containsKey(entry.getKey())) {
                    parentVerticesGraph.put(entry.getKey(), new HashSet<>());
                }
                parentVerticesGraph.get(node).add(entry.getKey());
            }
        }

        return parentVerticesGraph;
    }
}

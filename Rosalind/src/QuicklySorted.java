import java.util.Collections;
import java.util.List;

class QuicklySorted {

    void quickSort(List<Integer> array, int left, int right) {
        if (left < right) {
            int newPivotIndex = twoWayPartition(array, left, right, left);
            quickSort(array, left, newPivotIndex - 1);
            quickSort(array, newPivotIndex + 1, right);
        }

//        return array;
    }

    int quickSelect(List<Integer> array, int left, int right, int kSmallest) {
        if (left == right) {
            return array.get(left);
        }
        int newPivotIndex = twoWayPartition(array, left, right, left);
        if (kSmallest == newPivotIndex) {
            return array.get(kSmallest);
        } else if (kSmallest < newPivotIndex) {
            return quickSelect(array, left, newPivotIndex - 1, kSmallest);
        } else {
            return quickSelect(array, newPivotIndex + 1, right, kSmallest);
        }
    }

    int twoWayPartition(List<Integer> array, int left, int right, int pivotIndex) {
        int pivot = array.get(pivotIndex);
        Collections.swap(array, pivotIndex, right);
        int newPivotPosition = left;
        for (int i = left; i < right; i++) {
            if (array.get(i) <= pivot) {
                Collections.swap(array, i, newPivotPosition);
                newPivotPosition++;
            }
        }

        Collections.swap(array, newPivotPosition, right);
        return newPivotPosition;
    }

    List<Integer> threeWayPartition(List<Integer> array, int left, int right, int pivotIndex) {
        int pivot = array.get(pivotIndex);
        int i = left, j = left, k = right;

        while (j < k) {
            if (array.get(j) < pivot) {
                Collections.swap(array, i++, j++);
            } else if (array.get(j) > pivot) {
                Collections.swap(array, j, --k);
            } else {
                j++;
            }
        }

        return array;
    }
}
